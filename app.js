const express = require('express');
const app = express();
const PORT = 3000;

// GET Endpoint - http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(PORT, () => console.log(
    `Listening at http://localhost:${PORT}`
));

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
    const multiplicant = parseFloat(req.query.a);
    const multiplier = parseFloat(req.query.b);
    if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
    if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
    console.log({ multiplicant, multiplier });
    const product = multiply(multiplicant, multiplier);
    res.send(product.toString(10));
    } catch (err) {
    console.error(err.message);
    res.send("Couldn't calculate the product. Try again.");
    }
});

/**
 * This function calculates multiplication of two numbers.
* @param {number} multiplicant number being multiplied
* @param {number} multiplier number that multiplies
* @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
    if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
    const product = multiply(multiplicant, multiplier);
    return product;
}

/**
* This function calculates difference between two numbers.
* @param {number} minuend number being subtracted from
* @param {number} subtrahend number that is subtracted
* @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
    const difference = minuend - subtrahend;
    return difference;
};
